<?php

namespace ZfcUser\Mapper;

use Zend\Stdlib\Hydrator\ClassMethods;
use ZfcUser\Entity\UserInterface as UserEntityInterface;

class UserHydrator extends ClassMethods
{

    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     * @throws Exception\InvalidArgumentException
     */
    public function extract($object)
    {
        if (!$object instanceof UserEntityInterface) {
            throw new Exception\InvalidArgumentException('$object must be an instance of ZfcUser\Entity\UserInterface');
        }
        /* @var $object UserInterface */
        $data = parent::extract($object);
        if ($data['id'] !== null) {
            $data = $this->mapField('id', 'user_id', $data);
            // Adapt to customer database
            $data = $this->mapField('username', 'EL_NUMELEV', $data);
            $data = $this->mapField('email', 'EL_MAIL', $data);
            $data = $this->mapField('display_name', 'EL_NOM', $data);
            $data = $this->mapField('state', 'EL_ACTIF', $data);
        } else {
            unset($data['id']);
        }
        return $data;
    }

    /**
     * Hydrate $object with the provided $data.
     *
     * @param  array $data
     * @param  object $object
     * @return UserInterface
     * @throws Exception\InvalidArgumentException
     */
    public function hydrate(array $data, $object)
    {
        if (!$object instanceof UserEntityInterface) {
            throw new Exception\InvalidArgumentException('$object must be an instance of ZfcUser\Entity\UserInterface');
        }
        $data = $this->mapField('user_id', 'id', $data);
        // Adapt to customer database
        $data = $this->mapField('EL_NUMELEV', 'username', $data);
        $data = $this->mapField('EL_MAIL', 'email', $data);
        $data = $this->mapField('EL_NOM', 'display_name', $data);
        $data = $this->mapField('EL_ACTIF', 'state', $data);
        return parent::hydrate($data, $object);
    }

    protected function mapField($keyFrom, $keyTo, array $array)
    {
        $array[$keyTo] = $array[$keyFrom];
        unset($array[$keyFrom]);
        return $array;
    }
}
